!move_randomly.


+! move_randomly : true <- + randomly_moving
						  rover.ia.random(X);
						  rover.ia.random(Y);
						  rover.ia.add_journey(X,Y);
						  move(X,Y,1).



+ action_completed (move) : randomly_moving <- 	.print("I just finished moving.\nI am about to scan.");
												scan(2).

+ action_completed (move) : moving_to_gold <- .print("I am at the gold deposit").

+ resource_not_found : true <- .print("I did not find a resource");
								! move_randomly.
								
+ resource_found (T, Q, X, Y) : true <- - randomly_moving;
										+ moving_to_gold;
										.print ("I found a resource of type ",  T);
										rover.ia.add_journey(X,Y);
									        move(X,Y,1).


